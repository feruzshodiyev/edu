const Landing = [
    {
        path: '/landing',
        name: 'landing',
        meta: {
            layout: "landing"
        },
        component: () => import('@/views/Landing/index'),
    },
    {
        path: '/landing/all-courses',
        name: 'all-courses-landing',
        meta: {
            layout: "landing"
        },
        component: () => import('@/views/Landing/Courses/AllCourses'),
    },
    {
        path: '/landing/all-news',
        name: 'all-news-landing',
        meta: {
            layout: "landing"
        },
        component: () => import('@/views/Landing/News/AllNews'),
    },
    {
        path: '/landing/read-article',
        name: 'read-article-landing',
        meta: {
            layout: "landing"
        },
        component: () => import('@/views/Landing/News/ReadArticle'),
    },
]

export default Landing